﻿using System;

namespace ThreadsEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    class Employee
    {
        public string Name { get; set; }

        public Employee ReportsTo { get; set; }

        public Designation Designation { get; set; }

        public Employee(string name, Designation designation, Employee reportsTo)
        {
        }

        public Employee(Employee employee)
        {
            this.Name = employee.Name;
            this.ReportsTo = employee.ReportsTo;
            this.Designation = employee.Designation;
        }

    }

    enum Designation
    {
        CEO = 1,
        VP = 2,
        Manager = 3,
        DepartmentHead = 4,
        Staff = 5
    }
}
