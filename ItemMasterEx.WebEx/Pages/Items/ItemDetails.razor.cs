﻿using ItemMasterEx.DataTransferObjects;
using ItemMasterEx.Models.Primitives;
using ItemMasterEx.Services;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItemMasterEx.WebEx.Pages.Items
{
    public partial class ItemDetails
    {
        [Parameter]
        public long? Id { get; set; }

        [Parameter]
        public string Command { get; set; }

        [Inject]
        protected IItemService ItemService { get; set; }

        [Inject]
        public NavigationManager Navigator { get; set; }

        public ItemBase Item { get; private set; }

        public CreateItemRequestModel EditItem { get; private set; }

        public ItemDetails()
        {
            EditItem = new CreateItemRequestModel();
        }

        protected async override Task OnInitializedAsync()
        {
            if (Id.HasValue)
            {
                this.Item = await ItemService.GetItemById(this.Id.Value);
                this.EditItem = new CreateItemRequestModel(Item);
            }
        }

        public async Task HandleValidSubmit()
        {

            switch (Command)
            {
                case "edit":
                    await this.ItemService.Update(this.EditItem);
                    break;
                case "add":
                    await this.ItemService.Add(this.EditItem, new List<CreateItemRequestModel>());
                    break;
                default:
                    break;
            }

            Navigator.NavigateTo("items");
        }
    }
}
