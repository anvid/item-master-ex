﻿using ItemMasterEx.Models;
using ItemMasterEx.Models.Primitives;
using ItemMasterEx.RequestModels;
using ItemMasterEx.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.WebEx.Pages.Items
{
    public partial class ItemMaster
    {
        private const string ENTER = "Enter";

        [Inject]
        private IItemService ItemService { get; set; }

        public List<ItemModel> Items { get; set; }

        public List<ItemModel> ItemsCache { get; set; }

        public string SearchText { get; set; }

        public bool ShowDeletedItems { get; set; }

        public ItemMaster()
        {
            SearchText = string.Empty;
            ShowDeletedItems = false;
        }

        protected async override Task OnInitializedAsync()
        {
            await LoadItems(false);
            await base.OnInitializedAsync();
        }

        public void Filter(KeyboardEventArgs e)
        {
            if (e.Code == ENTER)
            {
                Search();
            }
        }

        public void ClearSearch()
        {
            this.SearchText = string.Empty;
            this.Items = new List<ItemModel>(ItemsCache);
        }

        public async Task DeleteItem(MouseEventArgs eventArgs, long id)
        {
            await this.ItemService.Delete(id);
            this.Items.Remove(this.Items.FirstOrDefault(x => x.Id == id));
        }

        private async Task LoadItems(bool showDeletedItems)
        {
            this.ItemsCache = new List<ItemModel>(await this.ItemService.Search(showDeletedItems, null));
            if (showDeletedItems)
            {
                this.Items = new List<ItemModel>(ItemsCache.OrderBy(x=>x.IsDeleted));
            }
            else
            {
                this.Items = new List<ItemModel>(ItemsCache.OrderBy(x=>x.Id));
            }
        }

        private void Search()
        {
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                this.Items = new List<ItemModel>(this.ItemsCache.Where(x => x.Name.Contains(SearchText, StringComparison.OrdinalIgnoreCase)));
            }
            else
            {
                this.Items = new List<ItemModel>(ItemsCache);
            }
        }

        public async Task OnShowDeletedItemsCheckboxChange(ChangeEventArgs e)
        {
            this.ShowDeletedItems = !ShowDeletedItems;
            await LoadItems(ShowDeletedItems);
        }

        public string GetDeletedRowStyle(ItemBase itemBase)
        {
            return itemBase.IsDeleted ? "deleted-row" : string.Empty;
        }
    }
}
