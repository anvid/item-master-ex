﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ItemMasterEx.DataTransferObjects;
using ItemMasterEx.Models;
using ItemMasterEx.RequestModels;
using ItemMasterEx.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace ItemMasterEx.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : AppControllerBase
    {
        private readonly IItemService itemService;

        public ItemController(IItemService itemService)
        {
            this.itemService = itemService;
        }

        // POST: api/Item
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ItemModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestObjectResult), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Search([FromQuery]SearchModel searchModel)
        {
            if (ModelState.IsValid)
            {
                return Ok(await itemService.Search(searchModel.Price));
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // GET: api/Item/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(await itemService.GetItemById(id));
        }

        // POST: api/Item
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateItemRequestModel model)
        {
            if (ModelState.IsValid)
            {
                return Ok(await itemService.Add(model, model.RelatedItems));
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch]
        [ProducesResponseType(typeof(ItemModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestObjectResult), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddAsRelatedItem(long mainItem, long relatedItem)
        {
            try
            {
                return Ok(await itemService.AddAsRelatedItem(mainItem, relatedItem));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestObjectResult), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put([FromBody] UpdateItemRequestModel model)
        {
            try
            {
                await itemService.Update(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(BadRequestObjectResult), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(long itemId)
        {
            try
            {
                await itemService.Delete(itemId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
