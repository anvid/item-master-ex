﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItemMasterEx.Common
{
    public interface IDeletable
    {
        bool IsDeleted { get; set; }
    }
}
