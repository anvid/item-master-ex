﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItemMasterEx.Common
{

    public abstract class EntityBase<TPrimaryKey>
    {
        public virtual TPrimaryKey Id { get; set; }

        public EntityBase()
        {
        }
    }

    public class DeletableEntity<TPrimaryKey> : EntityBase<TPrimaryKey>, IDeletable
    {
        public virtual bool IsDeleted { set; get; }

        public DeletableEntity()
        {
        }
    }
}
