﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Common
{
    public interface IDbContextProvider
    {
        DbContext Resolve();
    }
}
