﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ItemMasterEx.Common
{
    public interface IEntityStorageManager : IStorageManager
    {
        #region Public Methods

        IStorageManager Add<T>(IEnumerable<T> items) where T : class, new();

        IStorageManager Attach<T>(T item) where T : class, new();

        IStorageManager AttachAll<T>(IEnumerable<T> items) where T : class, new();

        DbSet<T> DataSet<T>() where T : class, new();

        IStorageManager DeleteAll<T>(IEnumerable<T> items, bool deleteVirtually) where T : class, IDeletable, new();

        IStorageManager DeleteAll<T>(IEnumerable<T> items) where T : class, new();

        IQueryResult<TEntity> GetAsPages<TEntity>(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        IEnumerable<TEntity> Get<TEntity>(
    Expression<Func<TEntity, bool>> filter,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        IQueryable<TEntity> GetAsQueryable<TEntity>(
    Expression<Func<TEntity, bool>> filter,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        Task<IEnumerable<TEntity>> GetAsync<TEntity>(
Expression<Func<TEntity, bool>> filter,
Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        Task<IQueryResult<TEntity>> GetAsPagesAsync<TEntity>(
    Expression<Func<TEntity, bool>> filter,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "", int? offset = null, int? take = null) where TEntity : class, new();

        Task<IEnumerable<TEntity>> In<TEntity, TKey>(Func<TEntity, TKey> field, params TKey[] values) where TEntity : class, new();

        Task<IEnumerable<T>> GetAsync<T>() where T : class, new();

        Task<T> GetAsync<T>(object key) where T : class, new();

        Task<T> GetAsync<T>(Expression<Func<T, bool>> predicate, params string[] navigationProperties) where T : class, new();

        Task<bool> SaveChangesAsync();

        IStorageManager UpdateAll<T>(IEnumerable<T> items) where T : class, new();

        #endregion Public Methods
    }
}
