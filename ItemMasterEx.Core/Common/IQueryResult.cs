﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Common
{
    public interface IQueryResult<T> where T : class, new()
    {
        IEnumerable<T> Data { get; set; }
        int TotalCount { get; set; }
        int FilteredCount { get; set; }
    }
}
