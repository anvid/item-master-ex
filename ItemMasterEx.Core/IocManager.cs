﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx
{
    public sealed class IocManager
    {
        private static IServiceProvider _instance;

        public static IServiceProvider Instance { get => _instance; }

        private IocManager()
        {
        }

        public static void Initialize(IServiceProvider serviceProvider)
        {
            if (_instance == null)
            {
                _instance = serviceProvider;
            }
        }
    }
}
