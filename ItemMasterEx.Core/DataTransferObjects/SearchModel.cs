﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.DataTransferObjects
{
    public class SearchModel : IValidatableObject
    {
        public double? Price { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Price.HasValue && Price.Value < 0)
            {
                yield return new ValidationResult("Price cannot be less than zero");
            }
        }
    }
}
