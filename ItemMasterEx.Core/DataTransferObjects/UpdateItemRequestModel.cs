﻿using ItemMasterEx.Models.Primitives;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.DataTransferObjects
{
    public class UpdateItemRequestModel : ItemBase, IValidatableObject
    {
        public UpdateItemRequestModel()
        {
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Specify any additional model validation logic here...

            if (Price < 0)
            {
                yield return new ValidationResult("Price cannot be less than zero");
            }

            if (Rank < 1)
            {
                yield return new ValidationResult("Rank cannot be less than one");
            }
        }
    }
}
