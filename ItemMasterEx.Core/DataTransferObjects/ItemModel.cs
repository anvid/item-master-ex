﻿using AutoMapper;
using ItemMasterEx.Models;
using ItemMasterEx.Models.Primitives;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.RequestModels
{
    [AutoMap(typeof(ItemBase))]
    [AutoMap(typeof(Item))]
    public class ItemModel : ItemBase
    {
        public ItemModel()
        {

        }

        public ItemModel(ItemBase item, IEnumerable<ItemBase> relatedItems)
            :base(item)
        {
            this.RelatedItems = relatedItems;
        }

        public IEnumerable<ItemBase> RelatedItems { get; set; }
    }
}
