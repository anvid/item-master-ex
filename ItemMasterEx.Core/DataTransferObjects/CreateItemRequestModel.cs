﻿using ItemMasterEx.Models;
using ItemMasterEx.Models.Primitives;
using ItemMasterEx.RequestModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.DataTransferObjects
{
    [AutoMapper.AutoMap(typeof(Item))]
    public class CreateItemRequestModel : ItemBase, IValidatableObject
    {
        public CreateItemRequestModel()
        {
            Id = 0;
            RelatedItems = new Collection<CreateItemRequestModel>();
        }

        public CreateItemRequestModel(ItemBase itemBase)
            :base(itemBase)
        {
            RelatedItems = new Collection<CreateItemRequestModel>();
        }

        public IEnumerable<CreateItemRequestModel> RelatedItems { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // TODO: Specify any additional model validation logic here...

            if (Price < 0)
            {
                yield return new ValidationResult("Price cannot be less than zero");
            }

            if (Rank < 1)
            {
                yield return new ValidationResult("Rank cannot be less than one");
            }
        }
    }
}
