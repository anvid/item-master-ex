﻿using AutoMapper;
using ItemMasterEx.Common;
using ItemMasterEx.DataTransferObjects;
using ItemMasterEx.Models;
using ItemMasterEx.Models.Primitives;
using ItemMasterEx.RequestModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace ItemMasterEx.Services
{
    public class ItemService : ServiceBase, IItemService
    {
        private const string PRICE_VALIDATION_ERROR = "Price cannot be less than zero";
        private const string RELATED_ITEMS = "RelatedItems";
        private const string NOT_FOUND = "Could not find the item";
        private const string MAIN_ITEM_NOT_FOUND = "Could not find the main item";
        private const string RELATED_ITEM_NOT_FOUND = "Could not find the specified related specified item";
        private const string ITEM_ALREADY_LINKED = "The specified related item is already linked with the main item";
        private const string CYCLIC_RELATIONSHIP = "Cannot create a cyclic relationship. The main item is a related item of the specified related item.";
        private readonly IEntityStorageManager storageManager;

        public ItemService(IEntityStorageManager storageManager)
        {
            this.storageManager = storageManager;
        }

        public async Task<ItemModel> Add(CreateItemRequestModel item, IEnumerable<CreateItemRequestModel> relatedItems)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var newItem = new Item(item);

            if (relatedItems != null)
            {
                AddRelatedItems(newItem, relatedItems);
            }

            storageManager.Add(newItem);

            await storageManager.SaveChangesAsync();

            return await Task.FromResult(new ItemModel(newItem, newItem.RelatedItems));
        }

        private Item AddRelatedItems(Item item, IEnumerable<CreateItemRequestModel> relatedItems)
        {
            if (relatedItems == null)
            {
                return item;
            }

            foreach (var relatedItem in relatedItems)
            {
                var relatedItemEntity = new Item(relatedItem);
                item.RelatedItems.Add(relatedItemEntity);
                AddRelatedItems(relatedItemEntity, relatedItem.RelatedItems);
            }

            return item;
        }

        public async Task<IEnumerable<ItemModel>> Search(bool showDeletedItems, double? price)
        {
            if (price < 0)
            {
                throw new ArgumentOutOfRangeException(PRICE_VALIDATION_ERROR);
            }

            if (price == null || price == 0)
            {
                var allResult = storageManager.GetAsQueryable<Item>(x => !showDeletedItems ? !x.IsDeleted : true,
                    x => x.OrderBy(x => x.Rank), RELATED_ITEMS);
                return (await allResult.ToListAsync()).Select(x=> new ItemModel(x, x.RelatedItems));
            }

            var query = storageManager.GetAsQueryable<Item>(x => x.Price == price && (!showDeletedItems ? !x.IsDeleted : true),
                x => x.OrderBy(x => x.Rank), RELATED_ITEMS);

            var r1 = To<IEnumerable<ItemBase>>(await query.ToListAsync());

            return await Task.FromResult(To<IEnumerable<ItemModel>>(r1));
        }

        public async Task<ItemModel> GetItemById(long id)
        {
            var item = await storageManager.GetAsync<Item>(x=>x.Id == id, "RelatedItems");
            return new ItemModel(item, item.RelatedItems);
        }

        public async Task<ItemModel> AddAsRelatedItem(long mainItem, long relatedItem)
        {
            if (mainItem == default)
            {
                throw new ArgumentException(nameof(mainItem));
            }

            if (relatedItem == default)
            {
                throw new ArgumentException(nameof(relatedItem));
            }

            var mainItemEntity = storageManager.Get<Item>(mainItem);
            
            var relatedItemEntity = storageManager.Get<Item>(relatedItem);

            if (mainItemEntity == null)
            {
                throw new ApplicationException(MAIN_ITEM_NOT_FOUND);
            }

            if (relatedItemEntity == null)
            {
                throw new ApplicationException(RELATED_ITEM_NOT_FOUND);
            }

            if (mainItemEntity.RelatedItems.Any(x=>x.Id == relatedItem))
            {
                throw new ApplicationException(ITEM_ALREADY_LINKED);
            }

            if (relatedItemEntity.RelatedItems.Any(x => x.Id == mainItem))
            {
                throw new ApplicationException(CYCLIC_RELATIONSHIP);
            }

            mainItemEntity.RelatedItems.Add(relatedItemEntity);

            await this.storageManager.SaveChangesAsync();

            return await Task.FromResult(To<ItemModel>(mainItemEntity));
        }

        public async Task Update(ItemBase item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            var itemEntity = storageManager.Get<Item>(item.Id);

            if (itemEntity == null)
            {
                throw new ApplicationException(NOT_FOUND);
            }

            if (item.Name != itemEntity.Name)
            {
                itemEntity.Name = item.Name;
            }

            if (item.Price != itemEntity.Price)
            {
                itemEntity.Price = item.Price;
            }

            if (item.Rank != itemEntity.Rank)
            {
                itemEntity.Rank = item.Rank;
            }

            await storageManager.SaveChangesAsync();
        }

        public async Task Delete(long itemId)
        {
            var itemEntity = storageManager.Get<Item>(itemId);

            if (itemEntity == null)
            {
                throw new ApplicationException(NOT_FOUND);
            }

            storageManager.Delete(itemEntity, true);

            await storageManager.SaveChangesAsync();
        }
    }
}
