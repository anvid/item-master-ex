﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ItemMasterEx.Services
{
    public class ServiceBase
    {
        protected IMapper Mapper { get; }

        public ServiceBase()
        {
            Mapper = IocManager.Instance.GetService<IMapper>();
        }

        protected TOut To<TOut>(object source)
        {
            return Mapper.Map<TOut>(source);
        }
    }
}
