﻿using ItemMasterEx.DataTransferObjects;
using ItemMasterEx.Models.Primitives;
using ItemMasterEx.RequestModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItemMasterEx.Services
{
    public interface IItemService
    {
        Task<ItemModel> Add(CreateItemRequestModel item, IEnumerable<CreateItemRequestModel> relatedItems);

        Task<ItemModel> AddAsRelatedItem(long mainItem, long relatedItem);

        Task<ItemModel> GetItemById(long id);

        Task<IEnumerable<ItemModel>> Search(bool showDeletedItems, double? price);

        Task Update(ItemBase item);

        Task Delete(long itemId);
    }
}