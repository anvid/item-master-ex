﻿using ItemMasterEx.Models.Primitives;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Models
{
    public class Item : ItemBase
    {
        public Item()
        {
            RelatedItems = new Collection<Item>();
        }

        public Item(ItemBase itemBase)
        {
            Id = itemBase.Id;
            Name = itemBase.Name;
            Price = itemBase.Price;
            IsDeleted = itemBase.IsDeleted;
            Rank = itemBase.Rank;
            RelatedItems = new Collection<Item>();
        }

        public virtual ICollection<Item> RelatedItems { get; set; }
    }
}
