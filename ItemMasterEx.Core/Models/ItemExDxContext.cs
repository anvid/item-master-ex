﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Models
{
    public class ItemExDxContext : DbContext
    {
        public DbSet<Item> Items { get; set; }


        public ItemExDxContext(DbContextOptions contextOptions)
            :base(contextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }
    }
}
