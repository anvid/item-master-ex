﻿using AutoMapper;
using ItemMasterEx.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Models.Primitives
{
    [AutoMap(typeof(Item))]
    abstract public class ItemBase : EntityBase<long>, IDeletable
    {

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }

        [Range(0, int.MaxValue)]
        public int? Rank { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public ItemBase()
        {
        }

        public ItemBase(ItemBase itemBase)
        {
            Id = itemBase.Id;
            Name = itemBase.Name;
            Rank = itemBase.Rank;
            IsDeleted = itemBase.IsDeleted;
            Price = itemBase.Price;
        }
    }
}
