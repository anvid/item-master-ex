﻿using ItemMasterEx.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItemMasterEx.Models
{
    public class AppDbContextProvider : IDbContextProvider
    {
        private readonly ItemExDxContext itemExDxContext;

        public AppDbContextProvider(ItemExDxContext itemExDxContext)
        {
            this.itemExDxContext = itemExDxContext;
        }

        public DbContext Resolve()
        {
            return itemExDxContext;
        }
    }
}
